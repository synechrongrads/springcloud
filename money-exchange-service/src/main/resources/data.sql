create table Exchange_Rate (
id int primary key,
currency_from varchar(50) not null,
currency_to varchar(50) not null,
conversion_multiple float not null,
application_port int not null
);

insert into Exchange_Rate (id,currency_from,currency_to,conversion_multiple,application_port)
values
(1001,'USD','CAD',1.45,8001);
insert into Exchange_Rate (id,currency_from,currency_to,conversion_multiple,application_port)
values
(1002,'CAD','EUR',12.45,8000);
insert into Exchange_Rate (id,currency_from,currency_to,conversion_multiple,application_port)
values
(1003,'INR','USD',82.45,8001);
insert into Exchange_Rate (id,currency_from,currency_to,conversion_multiple,application_port)
values
(1004,'SGD','AUD',3.56,8000);