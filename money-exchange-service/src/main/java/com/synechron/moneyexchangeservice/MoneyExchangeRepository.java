package com.synechron.moneyexchangeservice;


import org.springframework.data.jpa.repository.JpaRepository;

public interface MoneyExchangeRepository extends JpaRepository<ExchangeRate, Long> {

	ExchangeRate findByFromAndTo(String from, String to);
}
