package com.synechron.moneyexchangeservice;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MoneyExchangeController {
	
	
	@Autowired
	private Environment env;
	
	@Autowired
	private MoneyExchangeRepository repo;
	
	@GetMapping("/money-exchange/from/{from}/to/{to}")
	public ExchangeRate getExchangeRate(@PathVariable String from, @PathVariable String to) {
		ExchangeRate  exrate = repo.findByFromAndTo(from, to);
//		exrate.setPort(Integer.parseInt(env.getProperty("local.server.port")));
		return exrate;
	}
}
