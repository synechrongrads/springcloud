package com.synechron.moneyexchangeservice;

import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="Exchange_Rate")
public class ExchangeRate {
	@Id
	@Column(name="id")
	private Long exchangeid;

	@Column(name="currency_from")
	private String from;

	@Column(name="currency_to")
	private String to;
	
	@Column(name="conversion_multiple")
	private BigDecimal conversionMultiple;

	@Column(name="application_port")
	private int port;

public ExchangeRate() {
	// TODO Auto-generated constructor stub
}

public ExchangeRate(Long exchangeid, String from, String to, BigDecimal conversionMultiple) {
	this.exchangeid = exchangeid;
	this.from = from;
	this.to = to;
	this.conversionMultiple = conversionMultiple;
}

public Long getExchangeid() {
	return exchangeid;
}

public void setExchangeid(Long exchangeid) {
	this.exchangeid = exchangeid;
}

public String getFrom() {
	return from;
}

public void setFrom(String from) {
	this.from = from;
}

public String getTo() {
	return to;
}

public void setTo(String to) {
	this.to = to;
}

public BigDecimal getConversionMultiple() {
	return conversionMultiple;
}

public void setConversionMultiple(BigDecimal conversionMultiple) {
	this.conversionMultiple = conversionMultiple;
}

public int getPort() {
	return port;
}

public void setPort(int port) {
	this.port = port;
}



}
