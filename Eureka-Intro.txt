Against a directory service, if there is a query, then the Eureka namingserver provides a network service

Eureka Naming Server

REST-Based server that is used in AWS cloud
Each microservice registers itself with the Eureka Server
The naming server registers the client along with their port number and IP address
Default port is 8761


1. Create springboot app
2. dependencies : Eureka server, configclient, devtools, actuator

Steps to connect Microservice to Eureka Server

Step 1: Dependencies:- money-conversion-service

<!-- https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-eureka-client -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
    <version>4.1.0</version>
</dependency>

Step 2:
@SpringBootApplication
@EnableFeignClients
@EnableDiscoveryClient
public class MoneyConversionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoneyConversionServiceApplication.class, args);
	}

}
Step 3: update the application.properties
eureka.client.service-url.default.zone=http://localhost:8761/

