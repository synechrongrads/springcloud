package com.synechron.moneyconversionservice;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="money-exchange-service", path="/money-exchange", url="http://localhost:8000")
@RibbonClient(name="money-exchange-service")
public interface MoneyExchangeServiceProxy {
	@GetMapping("/from/{from}/to/{to}")
	MoneyConversion getExchangeRate(@PathVariable("from") String from, 
			@PathVariable("to") String to);
}
