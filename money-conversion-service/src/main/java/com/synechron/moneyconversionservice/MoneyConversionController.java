package com.synechron.moneyconversionservice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class MoneyConversionController {

	@Autowired
	private MoneyExchangeServiceProxy proxy;

	@GetMapping("/money-conversion/from/{from}/to/{to}/qty/{qty}")
	public MoneyConversion convertCurrency(@PathVariable String from, @PathVariable String to,
			@PathVariable float qty ) {

		Map<String, String> from_to_pathvariable = new HashMap<String, String>();
		from_to_pathvariable.put("from", from);
		from_to_pathvariable.put("to", to);
		ResponseEntity<MoneyConversion> response_entity = new RestTemplate().getForEntity("http://localhost:8001/money-exchange/from/{from}/to/{to}", MoneyConversion.class,from_to_pathvariable);
		MoneyConversion mc = response_entity.getBody();
		return new MoneyConversion(mc.getId(),from,to,mc.getConversionMultiple(),qty,
				qty*mc.getConversionMultiple(),mc.getPort());

	}

	@GetMapping("/money-conversion-feign/from/{from}/to/{to}/qty/{qty}")
	public MoneyConversion convertCurrencyFeign(@PathVariable String from, @PathVariable String to,
			@PathVariable float qty ) {

		MoneyConversion	mc = proxy.getExchangeRate(from, to);
		return new MoneyConversion(mc.getId(),from,to,mc.getConversionMultiple(),qty,
				qty*mc.getConversionMultiple(),mc.getPort());

	}
}
