package com.synechron.moneyconversionservice;

public class MoneyConversion {
private Long id;
private String from;
private String to;
private float conversionMultiple;
private float qty;
private float convertedAmount;
private int port;

public MoneyConversion() {
	// TODO Auto-generated constructor stub
}

public MoneyConversion(Long id, String from, String to, float conversionMultiple, float qty, float convertedAmount,
		int port) {
	this.id = id;
	this.from = from;
	this.to = to;
	this.conversionMultiple = conversionMultiple;
	this.qty = qty;
	this.convertedAmount = convertedAmount;
	this.port = port;
}

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public String getFrom() {
	return from;
}

public void setFrom(String from) {
	this.from = from;
}

public String getTo() {
	return to;
}

public void setTo(String to) {
	this.to = to;
}

public float getConversionMultiple() {
	return conversionMultiple;
}

public void setConversionMultiple(float conversionMultiple) {
	this.conversionMultiple = conversionMultiple;
}

public float getQty() {
	return qty;
}

public void setQty(float qty) {
	this.qty = qty;
}

public float getConvertedAmount() {
	return convertedAmount;
}

public void setConvertedAmount(float convertedAmount) {
	this.convertedAmount = convertedAmount;
}

public int getPort() {
	return port;
}

public void setPort(int port) {
	this.port = port;
}


}
