package com.synechron.limitsservice.bean;

public class Limitconfiguration {

	private int maximum_limit;
	private int minimum_limit;
	
	public Limitconfiguration() {
		// TODO Auto-generated constructor stub
	}

	public int getMaximum_limit() {
		return maximum_limit;
	}

	public void setMaximum_limit(int maximum_limit) {
		this.maximum_limit = maximum_limit;
	}

	public int getMinimum_limit() {
		return minimum_limit;
	}

	public void setMinimum_limit(int minimum_limit) {
		this.minimum_limit = minimum_limit;
	}

	public Limitconfiguration(int maximum_limit, int minimum_limit) {
		this.maximum_limit = maximum_limit;
		this.minimum_limit = minimum_limit;
	}
	
	
	
}
