package com.synechron.limitsservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.synechron.limitsservice.bean.Limitconfiguration;

@RestController
public class LimitsConfigurationController {

	@Autowired Configuration configuration;
	
	@GetMapping("/limits")
	public Limitconfiguration getLimitsFromConfiguration() {
		return new Limitconfiguration(configuration.getMaximum(), configuration.getMinimum());
	}
}
