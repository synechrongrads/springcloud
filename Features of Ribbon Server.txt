Features of Ribbon Server

1. Fault Tolerance
2. Load Balancing
3. Support for Cache and Batch
4. MPS (Multiple Protocol Support) in Async


Modules:

	ribbon: - API - that provides the features
	ribbon-loadbalancer: Load balancer API
	Eureka - Provides a dynamic server list for Spring Cloud
	transport: Uses protocols like HTTP, TCP, UDP for load balancing support
	http-client: REST Client
	core:Client config API

Steps:

1. Open the Money conversion project and add the dependencies to the pom.xml
	netflix-ribbon

<!-- https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-ribbon -->
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-ribbon</artifactId>
    <version>2.2.10.RELEASE</version>
</dependency>

2.Open the MoneyExchangeServiceProxy.java file and enable the Ribbon using @RibbonClient
along with that specify the name of the exchange service (money-exchange)

@FeignClient(name="money-exchange",url="http://localhost:8000/")
@RibbonClient(name="money-exchange")

3. Remove the URL attribute from the @FeignClient annotation

4. In application.properties include the following entries
	money-exchange.ribbon.listOfServers=http://localhost:8000,http://localhost:8001,http://localhost:8002


