package com.synechron.springcloudconfgserverlocal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class SpringCloudConfgServerLocalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudConfgServerLocalApplication.class, args);
	}

}
